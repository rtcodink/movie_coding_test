import 'package:flutter/material.dart';

Color bgColor = Color(0xFF1D2027);
Color greyColor = Color(0xFF696D74);
Color whiteColor = Color(0xFFFFFFFF);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
