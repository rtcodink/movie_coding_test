import 'package:flutter/material.dart';
import 'package:movie_coding_test/utils/theme.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget _header() {
      return AppBar(
        backgroundColor: bgColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Details Movie',
          style: TextStyle(color: whiteColor, fontSize: 18, fontWeight: medium),
        ),
      );
    }

    Widget _poster() {
      return Container(
        width: 315,
        height: 370,
        margin: EdgeInsets.only(top: 20, bottom: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          image: DecorationImage(
            image: AssetImage('assets/movie1.png'),
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    Widget _content() {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _poster(),
            Text(
              'Avatar (2009)',
              style: TextStyle(
                  color: whiteColor, fontSize: 20, fontWeight: medium),
            ),
            SizedBox(height: 8),
            Text(
              'Cast: Sam Worthington, Zoe Saldana',
              style: TextStyle(color: Color(0xffBABFC9), fontWeight: light),
            ),
            SizedBox(height: 30),
            Text(
              'Synopsis',
              style: TextStyle(
                  color: whiteColor, fontSize: 20, fontWeight: medium),
            ),
            SizedBox(height: 15),
            Text(
              'Diam risus, sed est amet, at porttitor tellus pulvinar molestie. Neque, turpis elementum pulvinar auctor gravida sed ut.',
              style: TextStyle(color: greyColor, fontWeight: light),
            ),
            SizedBox(height: 30),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
          child: ListView(
        children: [
          _header(),
          _content(),
        ],
      )),
    );
  }
}
