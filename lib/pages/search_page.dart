import 'package:flutter/material.dart';
import 'package:movie_coding_test/utils/theme.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: 45,
        margin: EdgeInsets.only(top: 38, left: 24, right: 24),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 11),
        decoration: BoxDecoration(
          color: Color(0xff282A3E),
          borderRadius: BorderRadius.circular(18),
        ),
        child: Center(
          child: Row(
            children: [
              Image.asset(
                'assets/search.png',
                width: 22,
              ),
              SizedBox(width: 16),
              Expanded(
                child: TextFormField(
                  style: TextStyle(
                    color: whiteColor,
                  ),
                  decoration: InputDecoration.collapsed(
                    hintText: 'Search movie...',
                    hintStyle: TextStyle(
                      color: greyColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
