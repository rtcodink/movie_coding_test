import 'package:flutter/material.dart';
import 'package:movie_coding_test/pages/search_page.dart';
import 'package:movie_coding_test/utils/theme.dart';
import 'package:movie_coding_test/widgets/movie_card.dart';
import 'package:movie_coding_test/widgets/tv_tile.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _header() {
      return Container(
        margin: EdgeInsets.only(top: 30, left: 24, right: 24),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Moviez',
                    style: TextStyle(
                      color: whiteColor,
                      fontSize: 32,
                      fontWeight: bold,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    'Watch the movies',
                    style: TextStyle(
                      fontSize: 16,
                      color: greyColor,
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchPage())),
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/search.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget _topMovies() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 24),
              child: Text(
                'Top Movies',
                style: TextStyle(
                    fontSize: 20, fontWeight: medium, color: whiteColor),
              ),
            ),
            SingleChildScrollView(
              padding: EdgeInsets.only(left: 24),
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  MovieCard(),
                  MovieCard(),
                  MovieCard(),
                  MovieCard(),
                ],
              ),
            )
          ],
        ),
      );
    }

    Widget _tvSeries() {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 24),
              child: Text(
                'TV Series',
                style: TextStyle(
                    fontSize: 20, fontWeight: medium, color: whiteColor),
              ),
            ),
            SingleChildScrollView(
              padding: EdgeInsets.only(left: 24),
              child: Column(
                children: [
                  TvTile(),
                  TvTile(),
                  TvTile(),
                ],
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: ListView(
          children: [
            _header(),
            _topMovies(),
            _tvSeries(),
          ],
        ),
      ),
    );
  }
}
