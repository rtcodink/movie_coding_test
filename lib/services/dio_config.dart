import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance = Dio();
createInstance() async {
  var options = BaseOptions(
      baseUrl:
          "https://imdb8.p.rapidapi.com/auto-complete?q=the%20dark%20knight",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "5c0babd8c8msh0dd632fef66d0f5p19a3d4jsn1604624f920d",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  dioInstance.options.baseUrl =
      "https://imdb8.p.rapidapi.com/auto-complete?q=the%20dark%20knight";
  return dioInstance;
}
