import 'package:flutter/material.dart';
import 'package:movie_coding_test/pages/detail_page.dart';
import 'package:movie_coding_test/utils/theme.dart';

class MovieCard extends StatelessWidget {
  const MovieCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => DetailPage())),
      child: Container(
        width: 190,
        height: 370,
        margin: EdgeInsets.only(top: 20, right: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 190,
              height: 250,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                image: DecorationImage(
                  image: AssetImage('assets/movie1.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Avatar',
              style: TextStyle(
                  fontSize: 18, color: whiteColor, fontWeight: medium),
            ),
            SizedBox(height: 5),
            Text(
              '2009',
              style: TextStyle(fontSize: 16, color: greyColor),
            ),
            SizedBox(height: 5),
            Text(
              'Sam Worthington, Zoe Saldana',
              style: TextStyle(fontSize: 16, color: greyColor),
            ),
          ],
        ),
      ),
    );
  }
}
