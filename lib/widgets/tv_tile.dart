import 'package:flutter/material.dart';
import 'package:movie_coding_test/pages/detail_page.dart';
import 'package:movie_coding_test/utils/theme.dart';

class TvTile extends StatelessWidget {
  const TvTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => DetailPage())),
      child: Container(
        width: double.infinity,
        height: 76,
        margin: EdgeInsets.only(top: 20),
        child: Row(
          children: [
            Container(
              width: 76,
              height: 76,
              margin: EdgeInsets.only(right: 12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: AssetImage('assets/movie2.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Money Heist',
                    style: TextStyle(
                        fontSize: 16, color: whiteColor, fontWeight: medium),
                  ),
                  SizedBox(height: 10),
                  Text(
                    '2017-2021',
                    style: TextStyle(fontSize: 12, color: greyColor),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Ursula Corbero, Alvaro Morte',
                    style: TextStyle(fontSize: 12, color: greyColor),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
